"""ferma URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin
from adverts.views import index as new_advert
from ferma import settings
from main.views import index as index
from main.views import search_results as search
from companies.views import index as companies
from django.conf.urls import handler404

handler404 = ''

urlpatterns = [
    url(r'^jet/', include('jet.urls', 'jet')),
    url(r'^admin/', admin.site.urls),
    url(r'^$', index, name='main'),
    url(r'^new_advert/$', new_advert, name='create_advert'),
    url(r'^ad/', include('adverts.urls', namespace='ad')),
    url(r'^ckeditor/', include('ckeditor_uploader.urls')),
    url(r'^search_results/$', search, name='search'),
    url(r'^companies/', include('companies.urls', namespace='companies')),
    url(r'^news/', include('news.urls', namespace='news'))
]
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
