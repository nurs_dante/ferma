# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2017-07-09 21:57
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('adverts', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='createnewadvert',
            name='main_image',
            field=models.ImageField(default=django.utils.timezone.now, upload_to='ad/media'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='media',
            name='media_file',
            field=models.FileField(upload_to='ad/media', verbose_name='\u041c\u0435\u0434\u0438\u0430 \u0444\u0430\u0439\u043b'),
        ),
    ]
