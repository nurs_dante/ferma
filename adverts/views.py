import os

from django.core.exceptions import ObjectDoesNotExist
from django.http import JsonResponse, Http404
from django.shortcuts import render

# Create your views here.
from adverts.forms import CreateNewAdvertForm
from adverts.models import Media, CreateNewAdvert
from ferma import settings
from main.views import slider, search_form


def index(request):
    fill_form = CreateNewAdvertForm(request.POST, files=request.FILES)
    create_new_ad = CreateNewAdvertForm(request.POST)
    context = {
        'fill_form': fill_form,
        'create_new_ad': create_new_ad
    }
    context.update(slider(request))
    context.update(search_form(request))
    return render(request, 'new_advert.html', context)


def one_ad(request, ad_id):
    ad = CreateNewAdvert.objects.get(id=ad_id)
    media = ad.media.all()
    context = {
        'ad': ad,
        'media': media
    }
    context.update(slider(request))
    context.update(search_form(request))
    return render(request, 'one_ad_page.html', context)


def ad_create(request):
    form = CreateNewAdvertForm(request.POST, files=request.FILES)
    if request.POST:
        if form.is_valid():
            new_ad = CreateNewAdvert()
            new_ad.title = form.cleaned_data['title']
            new_ad.name = form.cleaned_data['name']
            new_ad.email = form.cleaned_data['email']
            new_ad.advert_type = form.cleaned_data['advert_type']
            new_ad.categories = form.cleaned_data['categories']
            new_ad.text = form.cleaned_data['text']
            new_ad.agree_with_rules = True
            new_ad.is_active = False
            new_ad.save()
            if form.cleaned_data['removed_images']:
                removed_images = form.cleaned_data['removed_images'].split(',')
                for item in removed_images:
                    try:
                        r_media = Media.objects.get(id=int(item))
                        file_path = settings.MEDIA_ROOT + '/' + r_media.media_file.name
                        os.remove(file_path)
                        r_media.delete()
                    except ObjectDoesNotExist:
                        pass
            if form.cleaned_data['images']:
                images = form.cleaned_data['images'].split(',')
                for item in images:
                    try:
                        media = Media.objects.get(id=int(item))
                        new_ad.media.add(media)
                    except ObjectDoesNotExist:
                        pass
            return JsonResponse(dict(succes=True, message='Success'))
        else:
            return JsonResponse(dict(succes=True, message='Error1'))

    return JsonResponse(dict(succes=True, message='Error2'))


def upload_media(request):
    uploaded_files = list()
    files_count = len(request.FILES)
    for i in range(0, files_count):
        _file = request.FILES['file-' + str(i)]
        media = Media()
        media.media_file = _file
        media.save()
        uploaded_files.append({'id': media.id, 'url': media.media_file.url})
    return JsonResponse(dict(uploaded_files=uploaded_files))


def remove_uploaded_media(request):
    if request.POST:
        ids = request.POST.get('media_ids')
        ids = ids.split(',')
        for item in ids:
            try:
                media = Media.objects.get(id=int(item))
                file_path = settings.MEDIA_ROOT + '/' + media.media_file.name
                os.remove(file_path)
                media.delete()
            except ObjectDoesNotExist:
                pass

        return JsonResponse(dict(done=True))
    return JsonResponse(dict(done=False))
