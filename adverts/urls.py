from django.conf.urls import url
from adverts.views import ad_create, upload_media, remove_uploaded_media
from .views import one_ad as view_more

urlpatterns = [
    url(r'^ad_create/$', ad_create, name='ad_create'),
    url(r'^view_more/(?P<ad_id>\d+)$', view_more, name='view_more'),
    url(r'^upload_media/$', upload_media, name='upload_media'),
    url(r'^remove_uploaded_media/$', remove_uploaded_media, name='remove_uploaded_media'),
]
