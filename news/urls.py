from django.conf.urls import url

from .views import news_list

urlpatterns = [
    url(r'^$', news_list, name='news_list')
]
