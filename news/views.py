from django.shortcuts import render

# Create your views here.
from main.views import slider, search_form
from news.models import News


def news_list(request):
    news = News.objects.all()
    context = {
        'news': news
    }
    context.update(slider(request))
    context.update(search_form(request))
    return render(request, 'exept/news.html', context)
