# coding=utf-8
from django import forms

from main.models import SearchForm


class SearchForms(forms.ModelForm):
    class Meta:
        model = SearchForm
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(SearchForms, self).__init__(*args, **kwargs)
        self.fields['search_words'].widget.attrs.update(
            {'type': 'search', 'id': 'search_input', 'placeholder': 'Поиск'})
