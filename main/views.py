from django.core.exceptions import ObjectDoesNotExist
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.http import HttpResponse, JsonResponse, Http404
from django.shortcuts import render

# Create your views here.
from django.template import loader, Context

from adverts.models import CreateNewAdvert
from main.forms import SearchForms
from main.models import MainSlider, Banners

counter = 0


def index(request):
    context = {
    }
    context.update(slider(request))
    context.update(search_form(request))
    context.update(all_ads(request))
    context.update(sell_ads(request))
    context.update(change_ads(request))
    context.update(banners(request))
    return render(request, 'index.html', context)


def banners(request):
    banners = Banners.objects.all()
    context = {
        'banner': banners
    }
    return context


def slider(request):
    slider = MainSlider.objects.all()
    context = {
        'slider': slider
    }
    return context


def search_form(request):
    search_forms = SearchForms(request.POST)
    context = {
        'search_forms': search_forms
    }

    return context


def all_ads(request):
    all_ads = CreateNewAdvert.objects.filter(is_active=True)
    sell_adds_list = list()
    banner = Banners.objects.all()
    counter = 0
    adds_counter = 0
    for i in all_ads:
        sell_adds_list.append(i)
        adds_counter += 1
        if adds_counter % 3 == 0:
            if not counter > len(banner):
                sell_adds_list.append(banner[counter])
                counter += 1
            else:
                pass
    paginator = Paginator(sell_adds_list, 10)  # Show 25 contacts per page
    page = request.GET.get('page')
    try:
        all_ads_paginated = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        all_ads_paginated = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        all_ads_paginated = paginator.page(paginator.num_pages)

    for i in all_ads_paginated:
        print i
    context = {
        'all_ads': sell_adds_list,
    }
    return context


def sell_ads(request):
    sell = CreateNewAdvert.objects.filter(advert_type='sell', is_active=True)
    paginator = Paginator(sell, 10)  # Show 25 contacts per page
    page = request.GET.get('page')
    try:
        sell_paginated = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        sell_paginated = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        sell_paginated = paginator.page(paginator.num_pages)

    context = {
        'sell': sell_paginated,
    }
    return context


def change_ads(request):
    change = CreateNewAdvert.objects.filter(advert_type='change', is_active=True)
    paginator = Paginator(change, 10)  # Show 25 contacts per page
    page = request.GET.get('page')
    try:
        change_paginated = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        change_paginated = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        change_paginated = paginator.page(paginator.num_pages)

    context = {
        'change': change_paginated,
    }

    return context


def buy_ads(request):
    buy = CreateNewAdvert.objects.filter(advert_type='buy', is_active=True)
    paginator = Paginator(buy, 10)  # Show 25 contacts per page
    page = request.GET.get('page')
    try:
        buy_paginated = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        buy_paginated = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        buy_paginated = paginator.page(paginator.num_pages)

    context = {
        'buy': buy_paginated,
    }

    return context


def rent_ads(request):
    rent = CreateNewAdvert.objects.filter(advert_type='rent', is_active=True)
    paginator = Paginator(rent, 10)  # Show 25 contacts per page
    page = request.GET.get('page')
    try:
        rent_paginated = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        rent_paginated = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        rent_paginated = paginator.page(paginator.num_pages)

    context = {
        'rent': rent_paginated,
    }

    return context


def search_results(request):
    form = SearchForms(request.GET)
    if form.is_valid():
        if request.GET:
            search_words = form.cleaned_data['search_words']
            try:
                results = CreateNewAdvert.objects.filter(text__icontains=search_words)
                context = {
                    'results': results
                }
                context.update(slider(request))
                context.update(search_form(request))
                return render(request, 'src/_results.html', context)
            except ObjectDoesNotExist:
                return render(request, 'src/_no_results.html')

    else:
        return JsonResponse(dict(success=True, message='False'))
    return JsonResponse(dict(success=True, message='False'))
