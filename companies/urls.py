from django.conf.urls import url

from companies.views import index as companies

urlpatterns = [
    url(r'^', companies, name='companies_list')
]
