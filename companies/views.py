from django.shortcuts import render

# Create your views here.
from companies.models import Companies
from main.views import search_form


def index(request):
    companies = Companies.objects.all()
    context = {
        'companies': companies
    }
    context.update(search_form(request))
    return render(request, 'companies.html', context)
